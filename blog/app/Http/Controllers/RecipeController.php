<?php

namespace App\Http\Controllers;

use App\SaveRecipe;
use App\Recipe;
use App\SavedRecipe;
use Illuminate\Http\Request;
/*use Illuminate\Http\Response;
use Illuminate\Support\Facades\Storage;*/
use Illuminate\Support\Facades\File;

use Image;
use Elasticsearch;
use App\Http\Requests;
use Session;
use Auth;

class RecipeController extends Controller
{
    public function getIndex() {
    	$recipes = Recipe::all();
    	return view('home.index', ['recipes' => $recipes]);
    }

    public function search(){

        $data = [
            'body' => [
                'summary' => 'How to cook potato'
            ],
            'index' => 'recipes',
            'type' => 'recipe',
            'id' => '1',
        ];

        $return = Elasticsearch::index($data);
    }

    public function getSaveToProfile(Request $request, $id) {
    	$recipe = Recipe::find($id);
    	$oldRecipe = Session::has('saveRecipe') ? Session::get('saveRecipe') : null;
    	$saveRecipe = new SaveRecipe($oldRecipe);
    	$saveRecipe->save($recipe, $recipe->id);

    	$request->session()->put('saveRecipe', $saveRecipe);
    	
    	return redirect()->route('recipe.index');
    }

    public function getReduceByOne($id) {
        $oldRecipe = Session::has('saveRecipe') ? Session::get('saveRecipe') : null;
        $saveRecipe = new SaveRecipe($oldRecipe);
        $saveRecipe->reduceByOne($id);

         if(count($saveRecipe->items) > 0) {
            Session::put('saveRecipe', $saveRecipe);
        } else {
            Session::forget('saveRecipe');
        }
        
        return redirect()->route('recipe.saveRecipe');
    }

    public function getRemoveRecipe($id) {
        $oldRecipe = Session::has('saveRecipe') ? Session::get('saveRecipe') : null;
        $saveRecipe = new SaveRecipe($oldRecipe);
        $saveRecipe->removeItem($id);

        if(count($saveRecipe->items) > 0) {
            Session::put('saveRecipe', $saveRecipe);
        } else {
            Session::forget('saveRecipe');
        }

        return redirect()->route('recipe.saveRecipe'); 
    }

    public function getSaveRecipe() {
    	if(!Session::has('saveRecipe')) {
    		return view('home.save-recipe');
    	}
    	$oldRecipe = Session::get('saveRecipe');
    	$saveRecipe = new SaveRecipe($oldRecipe);

    	return view('home.save-recipe', ['recipes' => $saveRecipe->items, 'totalQty' => $saveRecipe->totalQty]);
    }

    public function getAddToProfile(Request $request){
        if(!Session::has('saveRecipe')) {
            return redirect()->route('recipe.index');
        }
        $oldRecipe = Session::get('saveRecipe');
        $saveRecipe = new SaveRecipe($oldRecipe);

        $savedRecipe = new SavedRecipe();
        $savedRecipe->saveRecipe = serialize($saveRecipe);

        Auth::user()->savedRecipes()->save($savedRecipe);

        Session::forget('saveRecipe');
        return redirect()->route('recipe.index')->with('success', 'Succesfully added to profile!');

    }

    public function getCreateRecipe() {
        return view('home.create-recipe');
    }

    public function postCreateRecipe(Request $request) {
        $this->validate($request, [
            'title' => 'required',
            'description' => 'required',
            'ingredients' => 'required',
            'cookingMethod' => 'required'
            ]);

        $recipe = new Recipe([
            'title' => $request->input('title'),
            'description' => $request->input('description'),
            'ingredients' => $request->input('ingredients'),
            'cookingMethod' => $request->input('cookingMethod')
        ]);

        if($request->hasFile('imagePath')){
            $image = $request->file('imagePath');
            $filename = time() . '.' . $image->getClientOriginalExtension();
            $location = public_path('images/' . $filename);
            Image::make($image)->save($location);

            $recipe->imagePath = $filename;
            
        }

        $recipe->save();

        return redirect()->route('recipe.index')->with('success', 'Succesfully created recipe!');
    }

}
