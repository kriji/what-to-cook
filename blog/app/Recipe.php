<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Recipe extends Model
{
     protected $fillable = ['imagePath', 'title', 'description', 'ingredients', 'cookingMethod'];
}
