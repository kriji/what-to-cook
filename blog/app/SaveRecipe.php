<?php

namespace App;


class SaveRecipe 
{
     public $items = null;
     public $totalQty = 0;

     public function __construct($oldRecipe) {
     	if($oldRecipe) {
     		$this->items = $oldRecipe->items;
     		$this->totalQty = $oldRecipe->totalQty;
     	}
     }

     public function save($item, $id) {
     	$savedRecipe = [
	     	'qty' => 0, 
	     	'item' => $item
     	];
     	
     	if($this->items) {
     		if(array_key_exists($id, $this->items)){
     			$savedRecipe = $this->items[$id];
     		}
     	}
     	$savedRecipe['qty']++;
     	$this->items[$id] = $savedRecipe;
     	$this->totalQty++;
     }

     public function reduceByOne($id){
          $this->items[$id]['qty']--;
          $this->totalQty--;

          if($this->items[$id]['qty'] <= 0){
               unset($this->items[$id]);
          }
     }

     public function removeItem($id){
          $this->totalQty -= $this->items[$id]['qty'];
          unset($this->items[$id]);
     }
}
