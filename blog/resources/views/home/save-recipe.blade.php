@extends('layouts.master')

@section('title')
	What to cook
@endsection

@section('content')
	@if(Session::has('saveRecipe'))

		<div class="row">
			<div class="col-sm-6 col-md-offset-3 col-sm-offset-3">
				<strong>Saved recipes: {{ $totalQty }}</strong>
			</div>
		</div>
		<hr>
	
		<div class="row">
			<div class="col-sm-6 col-md-offset-3 col-sm-offset-3">
				<ul class="list-group">
					@foreach($recipes as $recipe)
						<style type="text/css">
							div.thumbnail
							{
								 background-color: rgba(153, 255, 153, 0.2);
								 border-radius: 5px;
							}
						</style>

						<div class="thumbnail">
							<span class="badge">{{ $recipe['qty'] }}</span> 
							<strong>{{ $recipe['item']['title'] }}</strong>
							<p>{{$recipe['item']['description']}}</p>
							<div class="btn-group">
								<button type="button" class="btn btn-primary btn-xs dropdown-toogle" data-toggle="dropdown">Action <span class="caret"></span></button>
								<ul class="dropdown-menu">
									<li><a href="{{ route('recipe.reduceByOne', ['id' => $recipe['item']['id']] ) }}">Reduse by 1</a></li>
									<li><a href="{{ route('recipe.remove', ['id' => $recipe['item']['id']] ) }}">Reduse all</a></li>
								</ul>
							</div>
						</div>
					@endforeach
				</ul>
			</div>
		</div>
		<div class="row">
			<div class="col-sm-6 col-md-offset-3 col-sm-offset-3">
				<a href="{{ route('recipe.addToProfile') }}", type="button" class="btn btn-success">Add to profile</a>
			</div>
		</div>
	@else
		<div class="row">
			<div class="col-sm-6 col-md-offset-3 col-sm-offset-3">
				<h2>No saved recipes!</h2>
			</div>
		</div>
	@endif
@endsection