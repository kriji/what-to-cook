@extends('layouts.master')

@section('title')
	What to cook
@endsection

@section('content')
  @if(Session::has('success'))
    <div class="row">
      <div class="col-sm-6 col-md-4 col-md-offset-4 col-sm-offset-3">
        <div id="charge-message" class="alert alert-success">
          {{ Session::get('success') }}
        </div>
      </div>
    </div>
  @endif
  @foreach($recipes->chunk(3) as $recipeChunk)
  	<div class="row">
      @foreach($recipeChunk as $recipe)
        <div class="col-sm-6 col-md-4">
          <div class="thumbnail">
            <img src="{{ asset('images/' . $recipe->imagePath) }}" alt="..." class="img-responsive">
            <div class="caption">
              <h3>{{ $recipe->title }}</h3>
              <p class = "description">{{ $recipe->description }}</p>
              <div class="clearfix">
      	        <a href="{{ route('recipe.saveToProfile', ['id' => $recipe->id]) }}" class="btn btn-success pull-right" role="button"><i class="fa fa-paperclip" aria-hidden="true"></i></i> Save</a>
      	     </div>
            </div>
          </div>
        </div>
      @endforeach
  </div>
  @endforeach
@endsection