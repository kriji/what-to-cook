@extends('layouts.master')

@section('title')
	What to cook
@endsection

@section('content')
  <h1>Let's create a new recipe</h1>
  <hr>

  @if (count($errors) > 0)
        <div class="alert alert-danger">
          <strong>Whoops!</strong> There were some problems with your input.<br><br>
          <ul>
            @foreach ($errors->all() as $error)
              <li>{{ $error }}</li>
            @endforeach
          </ul>
        </div>
    @endif

  <form action="{{ route('recipe.createRecipe')}}" method="post" enctype="multipart/form-data">

    <div class="form-group">
      <label for="imagePath">Image</label>
      <input type="file" id="imagePath" name="imagePath" class="form-control">
    </div>

    <div class="form-group">
      <label for="title">Title</label>
       <input type="text" id="title" name="title" class="form-control">
    </div>


    <div class="form-group">
      <label for="description">Short description</label>
      <input type="text" id="description" name="description" class="form-control">
    </div>
    

    <div class="form-group">
      <label for="ingredients">Ingredients</label>
      <input type="text" id="ingredients" name="ingredients" class="form-control">
    </div>

    <div class="input-group">
       <label for="cookingMethod">Describe the method of cooking</label>
      <textarea for="cookingMethod", rows="4" cols="160", id="cookingMethod" name="cookingMethod" class="form-control"> </textarea> 
    </div>
<br> 
    <button type="submit" class="btn btn-success">Create recipe</button> <br><br><br>
    {{ csrf_field() }}
  </form>
@endsection