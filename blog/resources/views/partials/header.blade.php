<nav class="navbar navbar-default">
  <div class="container-fluid">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand" href="{{ route('recipe.index') }}">Brand</a>
    </div>

    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
      <form class="navbar-form navbar-left">
        <div class="form-group">
          <input type="text" class="form-control" placeholder="Search">
        </div>
        <button type="submit" class="btn btn-default">Submit</button>
      </form>
      <ul class="nav navbar-nav navbar-right">
        <li>
          <a href ="{{ route('recipe.createRecipe') }}"> 
            <i class="fa fa-plus" aria-hidden="true"></i> Create Recipe
          </a>
        </li>
        <li>
          <a href="{{ route('recipe.saveRecipe') }}">
            <i class="fa fa-paperclip" aria-hidden="true"></i> Saved
            <!-- Показваме бройката на запаметените рецепти -->
            <span class="badge">{{ Session::has('saveRecipe') ? Session::get('saveRecipe')->totalQty : '' }} </span>
          </a>
        </li>
        <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"> <i class="fa fa-user" aria-hidden="true"></i> User Managment<span class="caret"></span></a>
          <ul class="dropdown-menu">
          @if(Auth::check())
            <li><a href="{{ route('users.profile') }}"><i class="fa fa-user-plus" aria-hidden="true"></i> User profile</a></li>
            <li role="separator" class="divider"></li>
            <li><a href="{{ route('users.logout') }}"> <i class="fa fa-sign-out" aria-hidden="true"></i> Log Out</a></li>
            @else
              <li><a href="{{ route('users.signup') }}"><i class="fa fa-user-plus" aria-hidden="true"></i> Sign Up</a></li>
              <li role="separator" class="divider"></li>
              <li><a href="{{ route('users.signin') }}"><i class="fa fa-sign-in" aria-hidden="true"></i> Sign In</a></li>
            @endif
          </ul>
        </li>
      </ul>
    </div><!-- /.navbar-collapse -->
  </div><!-- /.container-fluid -->
</nav>