@extends('layouts.master')

@section('content')
	<div class="row">
		<div class=".col-md-4.col-md-offset-2">
			<h1>User Profile</h1>
			<hr>
			<h3>Added recipes</h3>
			@foreach($savedRecipes as $savedRecipe)
				@foreach($savedRecipe->saveRecipe->items as $item)

					<style type="text/css">
						li.list-group-item
						{
							 background-color: rgba(153, 255, 153, 0.1);
							 border-radius: 5px;
						}
					</style>

					 <li class="list-group-item">
							<strong>{{ $item['item']['title'] }}</strong>
							<p>{{$item['item']['description'] }}</p>
					</li> 
				@endforeach		
			@endforeach
		</div>	
	</div>
@endsection