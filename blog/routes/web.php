<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [
	'uses' => 'RecipeController@getIndex',
	'as' => 'recipe.index'	
]);


Route::get('/search', [
	'uses' => 'RecipeController@search',
	'as' => 'recipe.search'
]);

Route::get('/createRecipe', [
	'uses' => 'RecipeController@getCreateRecipe',
	'as' => 'recipe.createRecipe',
	'middleware' => 'auth'
]);

Route::post('/createRecipe', [
	'uses' => 'RecipeController@postCreateRecipe',
	'as' => 'recipe.createRecipe',
	'middleware' => 'auth'
]);

/*Route::get('/recipeImage/{filename}', [
	'uses' => 'RecipeController@getRecipeImage',
	'as' => 'recipe.imageRecipe'
]);*/

Route::get('/save-to-profile/{id}', [
	'uses' => 'RecipeController@getSaveToProfile',
	'as' => 'recipe.saveToProfile'
]);

Route::get('/reduce/{id}', [
	'uses' => 'RecipeController@getReduceByOne',
	'as' => 'recipe.reduceByOne'
]);	

Route::get('/remove/{id}', [
	'uses' => 'RecipeController@getRemoveRecipe',
	'as' => 'recipe.remove'
]);

Route::get('/add-to-profile', [
	'uses' => 'RecipeController@getAddToProfile',
	'as' => 'recipe.addToProfile',
	'middleware' => 'auth'
]);

Route::get('/save-recipe', [
	'uses' => 'RecipeController@getSaveRecipe',
	'as' => 'recipe.saveRecipe'
]);


/*Route::get('auth/facebook', 'Auth\RegisterController@redirectToProvider');
Route::get('auth/facebook/callback', 'Auth\RegisterController@handleProviderCallback');*/

Route::group(['prefix' => 'users'], function() {
	Route::group(['middleware' => 'guest'], function() {

		Route::get('/signup', [
			'uses' => 'UserController@getSignup',
			'as' => 'users.signup'
		]);

		Route::post('/signup', [
			'uses' => 'UserController@postSignup',
			'as' => 'users.signup'
		]);

		Route::get('/signin', [
			'uses' => 'UserController@getSignin',
			'as' => 'users.signin'
		]);

		Route::post('/signin', [
			'uses' => 'UserController@postSignin',
			'as' => 'users.signin'
		]);
	});

	Route::group(['middleware' => 'auth'], function(){

		Route::get('/profile', [
			'uses' => 'UserController@getProfile',
			'as' => 'users.profile'
		]);

		Route::get('/logout', [
			'uses' => 'UserController@getLogout',
			'as' => 'users.logout'
		]);
	});
});


Auth::routes();
/*
Route::get('/home', 'HomeController@index');
*/