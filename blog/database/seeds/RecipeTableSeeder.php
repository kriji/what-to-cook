<?php

use Illuminate\Database\Seeder;

class RecipeTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
       $recipe = new \App\Recipe([
       		'imagePath' => 'http://www.989wolf.com/wp-content/uploads/2016/05/maxresdefault-1.jpg',
       		'title' => 'Pizza',
       		'description' => 'Delicious pizza'
       	]);
       $recipe->save();
    }
}
